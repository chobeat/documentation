# Install

Mobilizon can be installed through different methods:

- [install a (precompiled) release](./release.md) (recommended)
  Supported systems: Debian Buster, Debian Bullseye, Ubuntu Jellyfish, Ubuntu Focal. Other systems may work.
- [run a Docker image](./docker.md) (standalone or with `docker-compose`)
- [install from source](./source.md)
